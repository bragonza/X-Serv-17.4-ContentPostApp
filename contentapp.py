#!/usr/bin/python3

"""
 contentApp class
 Simple web application for managing content

 Copyright Jesus M. Gonzalez-Barahona, Gregorio Robles 2009-2015
 jgb, grex @ gsyc.es
 TSAI, SAT and SARO subjects (Universidad Rey Juan Carlos)
 October 2009 - March 2015
"""

import webapp
formulario = """
    <br/><br/>
    <form action="" method = "POST"> 
    <input type="text" name="cuerpo" value="">
    <input type="submit" value="Enviar">
    </form>
"""


class contentApp(webapp.webApp):
    """Simple web application for managing content.

    Content is stored in a dictionary, which is intialized
    with the web content."""

    # Declare and initialize content
    content = {'/': 'Root page',
               '/page': 'A page'
               }

    def parse(self, request):
        """Return the resource name (including /)"""
        metodo = request.split(' ', 2)[0]
        recurso = request.split(' ', 2)[1]
        cuerpo = request.split('\r\n\r\n', 1)[1]

        return metodo, recurso, cuerpo

    def process(self, petition):

        """Process the relevant elements of the request.

        Finds the HTML text corresponding to the resource name,
        ignoring requests for resources not in the dictionary.
        """
        metodo, recurso, cuerpo = petition
        if metodo == "GET":
            if recurso in self.content.keys():
                httpCode = "200 OK"
                htmlBody = "<html><body>" + self.content[recurso]
            else:
                httpCode = "404 Not Found"
                htmlBody = "<html><body>" + recurso + " ha sido creado. dale un valor"
        else:
            self.content[recurso] = cuerpo.split("=")[1]
            httpCode = "200 OK"
            htmlBody = "<html><body>el valor de " + recurso + " es " + self.content[recurso]

        htmlBody += "<br> Introduce un nuevo valor " + formulario + "</body></html>"
        return (httpCode, htmlBody)


if __name__ == "__main__":
    testWebApp = contentApp("localhost", 1234)

